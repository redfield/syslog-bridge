// Copyright 2021 Assured Information Security, Inc. All Rights Reserved.

// +build windows

package main

import (
	"encoding/json"
	"log"
	"sync"

	"gopkg.in/mcuadros/go-syslog.v2"
	"golang.org/x/sys/windows/svc"
	"golang.org/x/sys/windows/svc/eventlog"
)

const addr = "[fd12:edf1:e1d0:1337::5]:514"

type handler struct {
	syslogChannel syslog.LogPartsChannel
	syslogHandler *syslog.ChannelHandler
	syslogServer *syslog.Server
	eventLog	*eventlog.Log
	paused		bool
	mu sync.Mutex
}

func (h *handler) initialize() error {
	var err error

	h.eventLog, err = eventlog.Open("Beam")
	if err != nil {
		return err
	}

	h.syslogChannel = make(syslog.LogPartsChannel)
	h.syslogHandler = syslog.NewChannelHandler(h.syslogChannel)

	h.syslogServer = syslog.NewServer()
	h.syslogServer.SetFormat(syslog.Automatic)
	h.syslogServer.SetHandler(h.syslogHandler)
	h.eventLog.Info(1, "Starting server on: " + addr)
	err = h.syslogServer.ListenUDP(addr)
	if err != nil {
		return err
	}

	h.paused = false

	err = h.syslogServer.Boot()
	if err != nil {
		return err
	}

	go func(channel syslog.LogPartsChannel) {
		for logParts := range channel {
			log.Println(logParts)
			jsonStr, _ := json.Marshal(logParts)
			h.mu.Lock()
			if h.paused == false {
				h.eventLog.Info(1, string(jsonStr))
			}
			h.mu.Unlock()
		}
	}(h.syslogChannel)

	return nil
}

func (h *handler) Execute(args []string, r <-chan svc.ChangeRequest, s chan<- svc.Status) (bool, uint32) {
	const accept = svc.AcceptStop | svc.AcceptShutdown | svc.AcceptPauseAndContinue

	s <- svc.Status{State: svc.StartPending}

	// ctx, cancel := context.WithCancel(context.Background())
	// if err := h.w.watch(ctx, false); err != nil {
	// 	return false, 1
	// }

	if err := h.initialize(); err != nil {
		return false, 1
	}

	s <- svc.Status{State: svc.Running, Accepts: accept}

	for {
		c, ok := <-r
		if !ok {
			return false, 1
		}

		switch c.Cmd {
		case svc.Interrogate:
			s <- c.CurrentStatus

		case svc.Stop, svc.Shutdown:
			s <- svc.Status{State: svc.StopPending}
			h.syslogServer.Kill()
			h.eventLog.Close()
			return false, 0

		case svc.Pause:
			h.mu.Lock()
			h.paused = true;
			h.mu.Unlock()
			s <- svc.Status{State: svc.Paused, Accepts: accept}

		case svc.Continue:
			h.mu.Lock()
			h.paused = false;
			h.mu.Unlock()
			s <- svc.Status{State: svc.Running, Accepts: accept}

		default:
			log.Printf("Unexpected control request #%d", c)
		}
	}
}


func main() {
	if ok, _ := svc.IsWindowsService(); ok {
		// Execute as a Windows Service
		_ = svc.Run("syslogbridge", &handler{})
		return
	}
}
