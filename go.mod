module gitlab.com/redfield/syslog-bridge

go 1.17

require (
	golang.org/x/sys v0.0.0-20211107104306-e0b2ad06fe42
	gopkg.in/mcuadros/go-syslog.v2 v2.3.0
)

require gopkg.in/check.v1 v1.0.0-20201130134442-10cb98267c6c // indirect
