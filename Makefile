GOPATH ?= $(HOME)/go

PATH := $(GOPATH)/bin:$(PATH)
DESTDIR ?= /


.PHONY: all
all: bins

.PHONY: bins
bins:
	GOOS=windows go build -o bin/windows/syslog-bridge.exe gitlab.com/redfield/syslog-bridge/cmd/syslog-bridge
